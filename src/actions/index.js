import { GET_NEWS } from './actionTypes';

// create Action

export const getNews = () => ({
    type: GET_NEWS
});