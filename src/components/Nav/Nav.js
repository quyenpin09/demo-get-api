import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

class Nav extends Component {
    render() {
        return (
            <div>
                <ul>
                    <li>
                        <NavLink to="/">Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/app">About</NavLink>
                    </li>
                </ul>

                <hr />

            </div>
        );
    }
}

export default Nav;