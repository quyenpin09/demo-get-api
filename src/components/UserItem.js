import React, { Component } from 'react';
const imgStyle = {
    height: 'auto',
    width: '80%',
    border: '4px solid #000',
    borderRadius: '5%'
};
const articleStyle = {
    width: '50%',
    margin: '0 auto',
    color: 'olive'
}

class UserItem extends Component {
    render() {
        console.log(this.props);
        return (
            this.props.user ?
                <article style={articleStyle} >
                    <div>
                        <h1>{this.props.user.first_name}</h1>
                        <img style={imgStyle} src={this.props.user.avatar} alt={this.props.user.first_name} />
                        <h4>{this.props.user.email}</h4>
                    </div>
                </article> :
                null
        );
    }
}

export default UserItem;