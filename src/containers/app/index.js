import React, {Component} from 'react';
import Button from '../../components/Button';
import UserItem from '../../components/UserItem'
import Loading from '../../components/Loading'
import {getNews} from "../../actions";
import {connect} from "react-redux";

class Index extends Component {

    render() {
        console.log(this.props);
        return (
            <div>
                <Button getNews={this.props.getNews} />
                <Loading loading={this.props.data.loading} />
                <UserItem user={this.props.data.users} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.getNewsReducers
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getNews: () => {
            dispatch(getNews());
        }
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(Index);