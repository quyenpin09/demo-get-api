import React, {Component} from 'react';
import {BrowserRouter as Router, Link} from 'react-router-dom';

class Index extends Component {

    render() {
        return (
            <Router>
                <div>
                    <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/app">About</Link>
                        </li>
                    </ul>

                    <hr />

                </div>
            </Router>
        );
    }
}


export default Index;