import React from 'react';
import ReactDOM from 'react-dom';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducers';
import rootSaga from './sagas';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Nav from "./components/Nav/Nav";
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware),
);

const loadContainer = (controller, params) => {
    const MyContainer = React.lazy(() => import('./containers/' + controller));

    return MyContainer;
};

const Spinner = () => {
    return <div>...</div>
}

const appRoot = (
    <Provider store={store}>
        <BrowserRouter>
            <Nav />
            <Switch>

                <Route path="/:controller/:action" component={(data) => {
                    const controller = data.match.params.controller + '/' + data.match.params.action;
                    console.log(controller);

                    const MyContainer = loadContainer(controller);
                    return (
                        <React.Suspense fallback={<Spinner />}>
                            <MyContainer/>
                        </React.Suspense>
                    );
                }}/>

                <Route path="/:controller" component={(data) => {
                    const controller = data.match.params.controller + '/index';
                    const MyContainer = loadContainer(controller);
                    return (
                        <React.Suspense fallback={<Spinner />}>
                            <MyContainer />
                        </React.Suspense>
                    );
                }}/>

                <Route exact path="/" component={(match) => {
                    const MyContainer = loadContainer('index/index');
                    return (
                        <React.Suspense fallback={<Spinner />}>
                            <MyContainer/>
                        </React.Suspense>
                    );

                }}/>

            </Switch>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(appRoot, document.getElementById('root'));

sagaMiddleware.run(rootSaga);

serviceWorker.unregister();
