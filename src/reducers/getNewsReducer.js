import { GET_NEWS, NEWS_RECEIVED } from '../actions/actionTypes';

const getNewsReducers = (state = {}, action) => {
    switch (action.type) {
        case GET_NEWS:
            return { ...state, loading: true };
        case NEWS_RECEIVED:
            return { ...state, users: action.json[0], loading: false };
        default:
            return state;
    }
};
export default getNewsReducers;