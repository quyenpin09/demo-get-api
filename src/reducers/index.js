import { combineReducers } from 'redux';
import getNewsReducers from './getNewsReducer';

const myReducer = combineReducers ({
    getNewsReducers
});

export default myReducer;