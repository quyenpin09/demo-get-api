import {GET_NEWS, NEWS_RECEIVED} from '../actions/actionTypes';
import { put, takeLatest } from 'redux-saga/effects';
function* fetchNews() {
    const json = yield fetch('https://reqres.in/api/users?page=2')
        .then(response => response.json(), );
    yield put({ type: NEWS_RECEIVED, json: json.data, });
    //yield console.log(json.data);
}
export function* actionWatcher() {
    yield takeLatest(GET_NEWS, fetchNews)
}