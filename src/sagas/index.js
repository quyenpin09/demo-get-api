import { all } from 'redux-saga/effects';
import { actionWatcher} from './getNewsSagas'
export default function* rootSaga() {
    yield all([ // gọi nhiều saga
        actionWatcher()
    ]);
}